package com.company;

public interface Calculadora {
    public double calculaArea(Circulo circulo);
    public double calculaArea(Retangulo retangulo);
    public double calculaArea(Triangulo triangulo);


}

