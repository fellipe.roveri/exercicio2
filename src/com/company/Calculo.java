package com.company;

public class Calculo implements Calculadora {

    InputOutput inputOutput;
    ValidarForma validarForma;

    public Calculo() {
        this.inputOutput = new InputOutput();
        this.validarForma = new ValidarForma();
    }

    @Override
    public double calculaArea(Circulo circulo) {
        return Math.pow(circulo.getRaio(), 2) * Math.PI;

    }

    @Override
    public double calculaArea(Retangulo retangulo) {
        return retangulo.getAltura() * retangulo.getLargura();
    }

    @Override
    public double calculaArea(Triangulo triangulo) {
        double resultado = 0;
        boolean isValido = validarForma.isValidoTriangulo();
        if (isValido) {
            double area = ((triangulo.getLadoA() + triangulo.getLadoB() + triangulo.getLadoC()) / 2);
            resultado = Math.sqrt(area * (area - triangulo.getLadoA()) * (area - triangulo.getLadoB()) *
                                   (area - triangulo.getLadoC()));
        }
        return resultado;
    }

}
