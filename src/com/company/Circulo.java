package com.company;

public class Circulo extends Forma{
    private double raio;

    public Circulo(double raio, int qtdLados) {
        super(qtdLados);
        this.raio = raio;
    }

    public double getRaio() {
        return raio;
    }

    public void setRaio(double raio) {
        this.raio = raio;
    }
}
