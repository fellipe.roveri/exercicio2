package com.company;

public abstract class Forma {

    private int qtdLados;

    public Forma(int qtdLados) {
        this.qtdLados = qtdLados;
    }

    public int getQtdLados() {
        return qtdLados;
    }

    public void setQtdLados(int qtdLados) {
        this.qtdLados = qtdLados;
    }
}
