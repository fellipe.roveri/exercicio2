package com.company;

public class FormasGeometricas {

    InputOutput inputOutput;
    Forma formato;
    ValidarForma validacao;
    Circulo circulo;
    Retangulo retangulo;
    Triangulo triangulo;
    Calculo calculo;


    public FormasGeometricas() {
        this.inputOutput = new InputOutput();
        this.validacao = new ValidarForma();
        this.calculo = new Calculo();



    }

    public void recebeFormaGeometrica (){
        int qtdLados = this.inputOutput.validaFormatoGeomatrico();
        int opcao = this.validacao.qualTipo(qtdLados, circulo, triangulo, retangulo);
        double area = 0;
        if (opcao == 1){
            area = calculo.calculaArea(circulo);
        }else if (opcao == 3){
            area = calculo.calculaArea(triangulo);
        }else if (opcao == 4){
            area = calculo.calculaArea(retangulo);
        }
        inputOutput.retornaArea(area);
    }




}
