package com.company;

public class ValidarForma {
    Calculo calculo;
    InputOutput inputOutput;
    private boolean circulo;
    private boolean triangulo;
    private boolean retangulo;

    public ValidarForma() {
        this.inputOutput = new InputOutput();
        this.circulo = false;
        this.triangulo = false;
        this.retangulo = false;
    }

    public int qualTipo(int qtdLados, Circulo circulo, Triangulo triangulo, Retangulo retangulo ) {
        if (qtdLados == 1) {
            circulo = new Circulo(inputOutput.medidaCirculo(), 1);
            double area = calculo.calculaArea(circulo);
            return 1;

        }else if(qtdLados == 3) {
            double[] valores = inputOutput.medidaTriangulo();
            double ladoA = valores[0];
            double ladoB = valores[1];
            double ladoC = valores[2];
            triangulo = new Triangulo(ladoA, ladoB, ladoC, 3);
            return 3;

        }else if(qtdLados == 4) {
            double[] valores = inputOutput.medidaTriangulo();
            double altura = valores[0];
            double largura = valores[1];
            retangulo = new Retangulo(altura, largura, 4);
            return 4;
        }
        return 0;
    }

    public boolean isValidoTriangulo() {

        double[] medidas = inputOutput.medidaTriangulo();
        double ladoA = medidas[0];
        double ladoB = medidas[1];
        double ladoC = medidas[2];
        if ((ladoA + ladoB > ladoC) &&
                (ladoA + ladoC > ladoB) &&
                (ladoB + ladoC > ladoA)){
            return true;

        }else{
            inputOutput.trianguloInvalido();
            return false;
        }

    }

    public boolean isRetangulo() {
        return retangulo;
    }


}
